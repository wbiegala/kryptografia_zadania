﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace szyfr_blokowy
{
    class Program
    {
        static void Main(string[] args)
        {
            BitArray input;
            BitArray key;


            // odczyt danych z klawiatury

            Console.WriteLine("Podaj 8-bitową liczbę do zaszyfrowania: ");
            string getInput = Console.ReadLine();
            Console.WriteLine();


            Console.WriteLine("Podaj 8-bitowy klucz: ");
            string getKey = Console.ReadLine();
            Console.WriteLine();

            try
            {
                input = StringToBool(getInput);
                key = StringToBool(getKey);
            }
            catch (InvalidCharException)
            {
                Console.WriteLine("Błędne znaki wpisane na wejściu programu!");
                Console.Read();
                return;
            }
            catch (InvalidLengthException)
            {
                Console.WriteLine("Błędna długość podanego ciągu!");
                Console.Read();
                return;
            }



            // obliczenia

            BitArray result = input;
            BitArray userKey = key;

            for (int i = 0; i < 8; i++)
            {
                BitArray roundKey = GenerateRoundKey(userKey, i, out userKey);
                BitArray temp1 = GetArrayByIndex(result, 0, 3);
                BitArray temp2 = GetArrayByIndex(result, 4, 7);

                temp1 = temp1.Xor(BlockS(temp2, roundKey));

                result = AddArrays(temp2, temp1);
            }

            // udostępnienie wyniku
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Wynik to: " + BoolToString(result));

            Console.ReadKey();
        }

        
        static BitArray GenerateRoundKey(BitArray userKey, int iteration, out BitArray KEY)
        {            

            BitArray temp;

            // 0, 2, 4, 6
            if ((iteration % 2) == 0)
            {
                BitArray temp1 = RotateLeft(GetArrayByIndex(userKey, 0, 3));
                BitArray temp2 = RotateLeft(GetArrayByIndex(userKey, 4, 7));

                temp = AddArrays(temp1, temp2);
            }
            // 1, 3, 5, 7
            else
            {
                temp = RotateLeft(userKey);
            }

            KEY = temp;

            BitArray result = new BitArray(4);
            result[0] = temp[0];
            result[1] = temp[2];
            result[2] = temp[4];
            result[3] = temp[6];

            return result;
        }
        

        static BitArray GetArrayByIndex(BitArray input, int first, int last)
        {
            if ((last < first) || (first < 0) || (last > input.Length - 1))
                throw new InvalidLengthException();

            BitArray result = new BitArray(last - first + 1);

            int l = 0;
            for (int i = first; i < last; i++)
            {
                result[l] = input[i];
                l++;
            }

            return result;
        }

        /// <summary>
        /// Joins two BitArrays into one
        /// </summary>
        /// <param name="older"></param>
        /// <param name="younger"></param>
        /// <returns></returns>
        static BitArray AddArrays(BitArray older, BitArray younger)
        {
            BitArray result = new BitArray(older.Length + younger.Length);
            int pointer = 0;

            for (int i = 0; i < older.Length; i++)
            {
                result[pointer] = older[i];
                pointer++;
            }

            for (int i = 0; i < younger.Length; i++)
            {
                result[pointer] = younger[i];
                pointer++;
            }

            return result;
        }

        /// <summary>
        /// Rotate bits to left side
        /// </summary>
        /// <param name="toRotate"></param>
        /// <returns></returns>
        static BitArray RotateLeft(BitArray toRotate)
        {
            BitArray result = new BitArray(toRotate.Length);

            for (int i = toRotate.Length - 1; i > 0; i--)
            {
                result[i - 1] = toRotate[i];
            }

            result[result.Length - 1] = toRotate[0];

            return result;
        }

        /// <summary>
        /// Calculates output on block S
        /// </summary>
        /// <param name="x">input</param>
        /// <param name="key">generated key</param>
        /// <returns>output</returns>
        static BitArray BlockS(BitArray x, BitArray key)
        {
            if (x.Length != 4)
                throw new InvalidLengthException();

            BitArray h = new BitArray(4);

            h[0] = true ^ x[2] ^ (x[0] & x[1]) ^ (x[0] & x[3]) ^ (x[1] & x[2] & x[3]) ^ (x[0] & x[1] & x[2] & x[3]) ^ key[0];
            h[1] = true ^ (x[0] & x[2]) ^ (x[0] & x[1] & x[2]) ^ (x[0] & x[2] & x[3]) ^ (x[0] & x[1] & x[2] & x[3]) ^ key[1];
            h[2] = x[1] ^ (x[0] & x[3]) ^ (x[0] & x[1] & x[3]) ^ (x[0] & x[1] & x[2] & x[3]) ^ key[2];
            h[3] = (x[0] & x[3]) ^ (x[1] & x[2]) ^ (x[0] & x[1] & x[3]) ^ (x[0] & x[2] & x[3]) ^ (x[0] & x[1] & x[2] & x[3]) ^ key[3];

            return h;
        }

        /// <summary>
        /// Converts string to BitArray.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        static BitArray StringToBool(string input)
        {
            BitArray result = new BitArray(8);

            for (int i = input.Length - 1; i >= 0; i--)
            {
                if (input[i] == '1')
                    result[i] = true;
                else if (input[i] == '0')
                    result[i] = false;
                else throw new InvalidCharException();
            }
            return result;
        }

        /// <summary>
        /// Converts BitArray to string.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Bool to string</returns>
        static string BoolToString(BitArray input)
        {
            string result = "";

            foreach (var item in input)
            {
                if (item.ToString() == "True")
                    result += "1";
                else
                    result += "0";
            }

            return result;
        }
    }


    class InvalidCharException : ApplicationException
    {

    }

    class InvalidLengthException : ApplicationException
    {

    }

}
